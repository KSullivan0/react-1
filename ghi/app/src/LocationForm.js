import React, { useEffect, useState } from 'react';

function LocationForm(props) {
    const [states, setStates] = useState([]);
    const [name, setName] = useState('');
    const [roomCount, setRoomCount] = useState('');
    const [city, setCity] = useState('');
    const [selectedState, setSelectedState] = useState('');

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/states/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setStates(data.states);
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    };

    const handleRoomCountChange = (event) => {
        const value = event.target.value;
        setRoomCount(value);
    };

    const handleCityChange = (event) => {
        const value = event.target.value;
        setCity(value);
    };

    const handleStateChange = (event) => {
        const value = event.target.value;
        setSelectedState(value);
    };

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.room_count = roomCount;
        data.name = name;
        data.city = city;
        data.state = selectedState;
        console.log(data);

        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            const newLocation = await response.json();
            console.log(newLocation);

            setName('');
            setRoomCount('');
            setCity('');
            setSelectedState('');
        }
    };

    return (
        <div>
            <h2>Create a new location</h2>
            <form onSubmit={handleSubmit} id="create-location-form">
                <input
                    onChange={handleNameChange}
                    placeholder="Name"
                    required
                    type="text"
                    name="name"
                    id="name"
                    className="form-control"
                    value={name} // Add value attribute for name input
                />
                <select
                    required
                    name="state"
                    id="state"
                    className="form-select"
                    value={selectedState}
                    onChange={handleStateChange}
                >
                    <option value="">Choose a state</option>
                    {states.map((state) => (
                        <option
                            key={state.abbreviation}
                            value={state.abbreviation}
                        >
                            {state.name}
                        </option>
                    ))}
                </select>
                <input
                    onChange={handleRoomCountChange}
                    placeholder="Room count"
                    required
                    type="number"
                    name="room_count"
                    id="room_count"
                    className="form-control"
                    value={roomCount} // Add value attribute for room count input
                />
                <input
                    onChange={handleCityChange}
                    placeholder="City"
                    required
                    type="text"
                    name="city"
                    id="city"
                    className="form-control"
                    value={city} // Add value attribute for city input
                />
                <button type="submit">Create</button>
            </form>
        </div>
    );
}

export default LocationForm;
